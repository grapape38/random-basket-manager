---
title: "Basket Manager Plots Over 100 Trials"
output:
 html_document:
    code_folding: hide
---
```{r}
library(ggplot2)
library(scales)
library(dplyr)
```
```{r}
lines <- read.csv('lines.csv')
lines$Date<-as.Date(paste(lines$Date,"-01",sep=""),format="%Y-%m-%d")

density <- read.csv('density.csv')

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2007-04-13'), Date < as.Date('2019-12-14'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "year", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2007-04-13 to 2019-12-14") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")

ggplot(density, aes(x=End.Equity,group=Strategy.Name)) +
	scale_x_continuous(labels=comma) +
	ggtitle('Density Plot n = 100') + geom_density(aes(color=Strategy.Name)) +
	theme(legend.direction = "vertical", 
				legend.position = "bottom",
				legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2007-04-13'), Date < as.Date('2008-02-08'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2007-04-13 to 2008-02-08") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2007-04-13 to 2008-02-08") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2008-02-08'), Date < as.Date('2008-12-05'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2008-02-08 to 2008-12-05") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2008-02-08 to 2008-12-05") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2008-12-05'), Date < as.Date('2009-10-02'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2008-12-05 to 2009-10-02") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2008-12-05 to 2009-10-02") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2009-10-02'), Date < as.Date('2010-07-30'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2009-10-02 to 2010-07-30") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2009-10-02 to 2010-07-30") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2010-07-30'), Date < as.Date('2011-05-27'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2010-07-30 to 2011-05-27") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2010-07-30 to 2011-05-27") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2011-05-27'), Date < as.Date('2012-03-23'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2011-05-27 to 2012-03-23") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2011-05-27 to 2012-03-23") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2012-03-23'), Date < as.Date('2013-01-18'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2012-03-23 to 2013-01-18") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2012-03-23 to 2013-01-18") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2013-01-18'), Date < as.Date('2013-11-15'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2013-01-18 to 2013-11-15") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2013-01-18 to 2013-11-15") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2013-11-15'), Date < as.Date('2014-09-12'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2013-11-15 to 2014-09-12") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2013-11-15 to 2014-09-12") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2014-09-12'), Date < as.Date('2015-07-10'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2014-09-12 to 2015-07-10") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2014-09-12 to 2015-07-10") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2015-07-10'), Date < as.Date('2016-05-06'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2015-07-10 to 2016-05-06") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2015-07-10 to 2016-05-06") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2016-05-06'), Date < as.Date('2017-03-03'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2016-05-06 to 2017-03-03") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2016-05-06 to 2017-03-03") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2017-03-03'), Date < as.Date('2017-12-29'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2017-03-03 to 2017-12-29") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2017-03-03 to 2017-12-29") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2017-12-29'), Date < as.Date('2018-10-26'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2017-12-29 to 2018-10-26") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2017-12-29 to 2018-10-26") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2018-10-26'), Date < as.Date('2019-08-23'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2018-10-26 to 2019-08-23") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2018-10-26 to 2019-08-23") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
```{r}
lines_filtered <- lines %>% filter(Date >= as.Date('2019-08-23'), Date < as.Date('2019-12-14'))

ggplot(lines_filtered, aes(x=Date, y= Average.Total.Equity, group=Strategy.Name)) +
    geom_line(aes(color=Strategy.Name)) +
    scale_x_date(breaks = "month", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from 2019-08-23 to 2019-12-14") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")


```
```{r}
profits <- lines_filtered %>% group_by(Strategy.Name) %>% 
	summarize(profit=last(Average.Total.Equity) - first(Average.Total.Equity))

ggplot(profits, aes(x=Strategy.Name, y = profit)) + 
	geom_col(aes(fill=Strategy.Name)) +
	ggtitle("Equity Bar Plot from 2019-08-23 to 2019-12-14") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal")

```
