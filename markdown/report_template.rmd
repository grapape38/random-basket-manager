---
title: "Basket Manager Plots Over {n} Trials"
output:
 html_document:
    code_folding: hide
---
```{{r warning=FALSE, message=FALSE}}
library(ggplot2)
library(scales)
library(dplyr)
```
