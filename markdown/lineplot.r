{var_name} <- lines %>% filter(Date >= as.Date('{start_date}'), Date < as.Date('{end_date}'))

ggplot({var_name}, aes(x=Date, y= {equity_column_name}, group={group_name})) +
    geom_line(aes(color={group_name})) +
    scale_x_date(breaks = "{breaks}", labels=date_format("%Y-%m")) +
      xlab("Year") +
        scale_y_continuous(labels=comma) +
          theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
            ggtitle("Strategy Line Plot from {start_date} to {end_date}") +
            theme(  legend.key = element_rect(),
                    legend.direction = "vertical", 
                    legend.position = "bottom",
                    legend.box = "horizontal")

