profits <- {var_name} %>% group_by({group_name}) %>% 
	summarize(profit=last({equity_column_name}) - first({equity_column_name}))

ggplot(profits, aes(x={group_name}, y = profit)) + 
	geom_col(aes(fill={group_name})) +
	ggtitle("Equity Bar Plot from {start_date} to {end_date}") +
	theme(legend.direction = "vertical", 
					legend.position = "bottom",
					legend.box = "horizontal",
					axis.text.x=element_blank())
