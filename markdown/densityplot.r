ggplot(density, aes(x=End.Equity,group=Strategy.Name)) +
	scale_x_continuous(labels=comma) +
	ggtitle('Density Plot n = {n}') + geom_density(aes(color=Strategy.Name)) +
	theme(legend.direction = "vertical", 
				legend.position = "bottom",
				legend.box = "horizontal")
