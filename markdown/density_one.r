ggplot(density, aes(x=End.Equity)) +
	scale_x_continuous(labels=comma) +
	ggtitle('Density Plot n = {n}') + geom_density()
