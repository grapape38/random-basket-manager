#!/bin/bash
cd ../
echo -e "library(knitr)\nknit('report.Rmd')" > "$1"/createMD.R
docker run -ti -v "$PWD/$1":/scripts -w "/scripts" r-env Rscript ./createMD.R
rm "$1"/createMD.R
