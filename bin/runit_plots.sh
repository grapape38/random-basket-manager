#!/bin/bash

RUN_NAME=$1
./runit.sh $@ --plots

OUTFILES_DIR=/opt/blocktradingsystems/outfiles
PLOT_DIR=$OUTFILES_DIR/plots
WORK_DIR=../work

PLOT_OUT_DIR=$PLOT_DIR/$RUN_NAME
rm -rf $PLOT_OUT_DIR
mkdir $PLOT_OUT_DIR
echo ""
echo "Running R ..."
echo -e "library(knitr)\nknit('report.Rmd')" > "$WORK_DIR"/createMD.R
docker run -ti -v "$PWD/$WORK_DIR":/scripts -w "/scripts" r-env Rscript ./createMD.R
rm "$WORK_DIR"/createMD.R
cp $WORK_DIR/figure/unnamed-chunk-2-1.png $PLOT_OUT_DIR/density.png
cp $WORK_DIR/figure/unnamed-chunk-3-1.png $PLOT_OUT_DIR/lines.png
