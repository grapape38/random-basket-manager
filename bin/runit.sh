#!/bin/bash

. ./setenv

EQUITY=100000
NUM_RUNS=100
RISKPCT=1
RUN_NAME=default
mkdir $WORK_DIR

PLOT=0
MULTI=0
while test $# -gt 0
do
    case "$1" in
        --plots)
           PLOT=1
           ;;
        --test-data) 
           TESTDATA_FILE="${2}"
            ;;
        --multi) 
           MULTI=1
            ;;
        --run-name) 
           RUN_NAME="${2}"
            ;;
        --i) 
           INTERVAL="-i ${2}"
            ;;
    esac
    shift
done

if [ $MULTI -eq 0 ]; then 
   if [ -z ${TESTDATA_FILE+x} ]; then 
      echo "--test-data is required"
      echo "--multi is for multiple strategies"
      cd $OUTFILES_DIR && find -name '*trades.csv'
      exit -1
   fi   
fi


if [ $MULTI -eq 0 ]; then
   CMD="$BASE_DIR/random_basket -e $EQUITY -f "$OUTFILES_DIR/$TESTDATA_FILE.csv" -n $NUM_RUNS -r1 -o $WORK_DIR $INTERVAL"
else
   CMD="$BASE_DIR/random_basket -e $EQUITY -d "$OUTFILES_DIR/test/$RUN_NAME" -n $NUM_RUNS -r$RISKPCT -o $WORK_DIR $INTERVAL"
fi

echo "running $CMD"

if [ $PLOT -eq 1 ]; 
then
    $CMD --plots
    echo ""
    echo "Running R ..."
    echo -e "library(rmarkdown)\nrender('report.Rmd')" > "$WORK_DIR"/createMD.R
    docker run -ti -v "$PWD/$WORK_DIR":/scripts -w "/scripts" r-env Rscript ./createMD.R
    rm "$WORK_DIR"/createMD.R
 else    
    $CMD
fi
