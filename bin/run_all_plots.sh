#!/bin/bash

. ./setenv

function run_it {
   run_name=$1
   ./runit.sh --multi --plots --run-name $run_name --i 300
   cp $WORK_DIR/report.html $PLOT_DIR/$run_name.html
   echo "wrote $PLOT_DIR/$run_name.html"
}

run_it 500-5.5
run_it 500-5.0
run_it 500-4.5
run_it 500-4.0
run_it 500-3.5
run_it 500-3.0
run_it 500-2.5
run_it 500-2.0
run_it 500-1.5
run_it 500-1.0
run_it 500-0.5
run_it 500-0.0
