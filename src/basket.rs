use chrono::NaiveDate;
use rand::Rng;

mod my_date_format {
    use chrono::NaiveDate;
    use serde::{self, Deserialize, Deserializer};

    const FORMAT: &'static str = "%m/%d/%Y";

    pub fn deserialize<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        NaiveDate::parse_from_str(&s, FORMAT).map_err(serde::de::Error::custom)
    }
}

#[derive(Debug, Deserialize)]
pub struct Trade {
    symbol: String,
    #[serde(rename = "entryDate", with = "my_date_format")]
    entry_date: NaiveDate,
    #[serde(rename = "exitDate", with = "my_date_format")]
    exit_date: NaiveDate,
    #[serde(rename = "entryPrice")]
    entry_price: f32,
    #[serde(rename = "exitPrice")]
    exit_price: f32,
    #[serde(rename = "initial stop")]
    stop_price: f32,
}

/*impl Trade {
    fn new(symbol: String, entry_price: f32, exit_price: f32,
           stop_price: f32, entry_date: NaiveDate, exit_date: NaiveDate) -> Self {
        Trade { symbol, entry_price, exit_price, stop_price, entry_date, exit_date }
    }
}*/

#[derive(Debug)]
pub struct TradeEvent {
    symbol: String,
    price: f32,
    date: NaiveDate,
}

impl TradeEvent {
    fn new(symbol: String, price: f32, date: NaiveDate) -> Self {
        TradeEvent {
            symbol,
            price,
            date,
        }
    }
}

#[derive(Debug)]
pub struct EntryEvent {
    trade: TradeEvent,
    stop_price: f32,
}

impl EntryEvent {
    fn new(symbol: String, entry_price: f32, stop_price: f32, date: NaiveDate) -> Self {
        EntryEvent {
            trade: TradeEvent::new(symbol, entry_price, date),
            stop_price,
        }
    }
}

#[derive(Debug)]
pub enum ETradeEvent {
    Entry(EntryEvent),
    Exit(TradeEvent),
}

fn new_entry(entry: EntryEvent) -> ETradeEvent {
    ETradeEvent::Entry(entry)
}

fn new_exit(exit: TradeEvent) -> ETradeEvent {
    ETradeEvent::Exit(exit)
}

impl ETradeEvent {
    fn date(&self) -> NaiveDate {
        match self {
            ETradeEvent::Entry(ev) => ev.trade.date,
            ETradeEvent::Exit(tr) => tr.date,
        }
    }
}

#[derive(Debug, Clone)]
pub struct OwnStock {
    symbol: String,
    entry_price: f32,
    units: u32,
}

#[derive(Clone)]
pub struct BasketSnapshot {
    pub date: NaiveDate,
    cur_stocks: Vec<OwnStock>,
    avail_equity: f32,
    entry_equity: f32,
    pub stats: BasketStats,
}

impl std::fmt::Debug for BasketSnapshot {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Date: {:?}\n", self.date)?;
        write!(f, "Cash Equity: {:?}\n", self.avail_equity)?;
        write!(f, "Entry Equity: {:?}\n", self.entry_equity)?;
        write!(
            f,
            "Total Equity: {:?}\n",
            self.avail_equity + self.entry_equity
        )?;
        for stock in &self.cur_stocks {
            write!(f, "symbol: {}, units: {}\n", stock.symbol, stock.units)?;
        }
        std::fmt::Result::Ok(())
    }
}

impl BasketSnapshot {
    fn snap(date: NaiveDate, state: &BasketState) -> Self {
        BasketSnapshot {
            date,
            cur_stocks: state.cur_stocks.clone(),
            avail_equity: state.equity,
            entry_equity: state.entry_equity(),
            stats: state.stats.clone(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct BasketStats {
    pub num_win: u32,
    pub num_loss: u32,
    pub total_win: f32,
    pub total_loss: f32,
}

impl BasketStats {
    pub fn print(&self, init_equity: f32) {
        println!(
            "End Equity: {}",
            init_equity + self.total_win - self.total_loss
        );
        println!("Number of wins: {}", self.num_win);
        println!("Number of losses: {}", self.num_loss);
        println!("Average win: {}", self.total_win / self.num_win as f32);
        println!("Average loss: {}", self.total_loss / self.num_loss as f32);
    }
    pub fn end_equity(&self, init_equity: f32) -> f32 {
        init_equity + self.total_win - self.total_loss
    }
}

#[derive(Serialize, Clone)]
pub struct BasketStatsSummary {
    pub equity: f32,
    #[serde(rename = "numTrades")]
    pub num_trades: u32,
    #[serde(rename = "pctWin")]
    pub pct_win: f32,
    #[serde(rename = "pctLoss")]
    pub pct_loss: f32,
    #[serde(rename = "avgWin")]
    pub avg_win: f32,
    #[serde(rename = "avgLoss")]
    pub avg_loss: f32,
    #[serde(rename = "numLosses")]
    pub num_losses: u32,
    #[serde(rename = "numWins")]
    pub num_wins: u32,
}

impl BasketStatsSummary {
    pub fn new(stats: &BasketStats, init_equity: f32) -> Self {
        let equity = init_equity + stats.total_win - stats.total_loss;
        let (num_win, num_loss) = (stats.num_win as f32, stats.num_loss as f32);
        let avg_win = stats.total_win / num_win;
        let avg_loss = stats.total_loss / num_loss;
        let pct_win = num_win / (num_win + num_loss);
        let pct_loss = num_loss / (num_win + num_loss);
        BasketStatsSummary {
            equity,
            num_trades: stats.num_win + stats.num_loss,
            num_wins: stats.num_win,
            num_losses: stats.num_loss,
            pct_win,
            pct_loss,
            avg_win,
            avg_loss,
        }
    }
}

#[derive(Serialize, Clone)]
pub struct TrialBasketStatsSummary {
    pub best: BasketStatsSummary,
    pub worst: BasketStatsSummary,
    pub average: BasketStatsSummary,
}

pub type BasketHistory = Vec<BasketSnapshot>;

#[derive(Clone)]
pub struct BasketState {
    pub equity: f32,
    risk_pct: f32,
    cur_stocks: Vec<OwnStock>,
    pub stats: BasketStats,
    history: BasketHistory,
}

impl BasketState {
    pub fn new(equity: f32, risk_pct: f32) -> Self {
        BasketState {
            equity,
            risk_pct,
            ..BasketState::default()
        }
    }
}

impl Default for BasketState {
    fn default() -> Self {
        BasketState {
            equity: 0.,
            cur_stocks: Vec::new(),
            history: Vec::new(),
            risk_pct: 0.,
            stats: BasketStats {
                num_win: 0,
                num_loss: 0,
                total_win: 0.,
                total_loss: 0.,
            },
        }
    }
}

pub fn get_events(trades: &[Trade]) -> Vec<ETradeEvent> {
    let mut events = Vec::new();
    for t in trades {
        let entry_event = new_entry(EntryEvent::new(
            t.symbol.clone(),
            t.entry_price,
            t.stop_price,
            t.entry_date,
        ));
        let exit_event = new_exit(TradeEvent::new(t.symbol.clone(), t.exit_price, t.exit_date));
        events.push(entry_event);
        events.push(exit_event);
    }
    events
}

impl BasketState {
    fn entry_equity(&self) -> f32 {
        self.cur_stocks
            .iter()
            .fold(0., |sum, s| sum + s.entry_price * (s.units as f32))
    }
    fn total_equity(&self) -> f32 {
        self.equity + self.entry_equity()
    }
    fn get_trade_units(&self, entry: &EntryEvent) -> u32 {
        (self.total_equity() * (self.risk_pct / 100.) / (entry.trade.price - entry.stop_price))
            as u32
    }
    fn add_stock(&mut self, symbol: String, entry_price: f32, units: u32) {
        self.cur_stocks.push(OwnStock {
            symbol,
            entry_price,
            units,
        });
        self.equity -= entry_price * units as f32;
    }
    fn remove_stock(&mut self, pos: usize, exit_price: f32) {
        let stock = &self.cur_stocks[pos];
        self.equity += stock.units as f32 * exit_price;
        let change = stock.units as f32 * (exit_price - stock.entry_price);
        if stock.entry_price <= exit_price {
            self.stats.num_win += 1;
            self.stats.total_win += change;
        } else {
            self.stats.num_loss += 1;
            self.stats.total_loss -= change;
        }
        self.cur_stocks.remove(pos);
    }
    pub fn simulate(&mut self, events: &Vec<ETradeEvent>) {
        let mut rng = rand::thread_rng();
        let record_event_interval = (events.len() / 2000).max(1);
        for (num_event, ev) in events.iter().enumerate() {
            match ev {
                ETradeEvent::Entry(ref entry) => {
                    let units = self.get_trade_units(entry);
                    if units as f32 * entry.trade.price <= self.equity {
                        let r: f64 = rng.gen();
                        if r > 0.5 {
                            self.add_stock(entry.trade.symbol.clone(), entry.trade.price, units);
                        }
                    }
                }
                ETradeEvent::Exit(ref exit) => {
                    if let Some(pos) = self.cur_stocks.iter().position(|s| s.symbol == exit.symbol)
                    {
                        self.remove_stock(pos, exit.price);
                    }
                }
            }
            if (num_event % record_event_interval == 0) || (num_event == events.len() - 1) {
                self.history.push(BasketSnapshot::snap(ev.date(), self));
            }
        }
    }

    pub fn print_history(&self) {
        for snap in &self.history {
            println!("{:?}", snap);
        }
    }
    pub fn print_stats(&self) {
        let avg_win = self.stats.total_win / (self.stats.num_win as f32);
        let avg_loss = self.stats.total_loss / (self.stats.num_loss as f32);
        println!("Remaining Equity {:?}", self.total_equity());
        println!("Number of wins: {:?}", self.stats.num_win);
        println!("Number of losses: {:?}", self.stats.num_loss);
        println!("Average win: {:?}", avg_win);
        println!("Average loss: {:?}", avg_loss);
    }
}

pub fn run_trials(basket: BasketState, trades: &Vec<Trade>, n_trials: u32) -> Vec<BasketHistory> {
    let mut events = get_events(&trades);
    events.sort_by_key(|ev| ev.date());
    let mut trial_stats = Vec::new();
    for _ in 0..n_trials {
        let mut b = basket.clone();
        b.simulate(&events);
        trial_stats.push(b.history);
    }
    trial_stats
}

#[derive(Debug)]
pub struct AvgBasketSnap {
    pub date: NaiveDate,
    pub equity: f32,
}

impl AvgBasketSnap {
    fn new(date: NaiveDate, equity: f32) -> Self {
        AvgBasketSnap { date, equity }
    }
}

pub type AvgBasketHistory = Vec<AvgBasketSnap>;

#[derive(Debug)]
pub struct IntervalInfo {
    pub start_date: NaiveDate,
    pub end_date: NaiveDate,
    pub day_interval: Option<i64>,
}

impl IntervalInfo {
    pub fn new(start_date: NaiveDate, end_date: NaiveDate, day_interval: Option<i64>) -> Self {
        IntervalInfo {
            start_date,
            end_date,
            day_interval,
        }
    }
    pub fn breaks(&self) -> &'static str {
        match self.day_interval {
            Some(0..=50) => "day",
            Some(51..=1500) => "month",
            _ => "year",
        }
    }
}

pub trait GetDate {
    fn get_date(&self) -> NaiveDate;
}

impl GetDate for BasketSnapshot {
    fn get_date(&self) -> NaiveDate {
        self.date
    }
}

impl GetDate for AvgBasketSnap {
    fn get_date(&self) -> NaiveDate {
        self.date
    }
}

/*pub struct DateInterval {
    days: u32,
    months: u32,
    years: i32
}

impl DateInterval {
    pub fn new(days: u32, months: u32, years: i32) -> DateInterval {
        DateInterval {
            days, months, years
        }
    }
}*/

pub fn avg_trials(init_equity: f32, trial_stats: &Vec<BasketHistory>) -> AvgBasketHistory {
    let mut avg_history: Vec<AvgBasketSnap> = Vec::new();
    if trial_stats.is_empty() {
        return avg_history;
    }
    for snap in &trial_stats[0] {
        avg_history.push(AvgBasketSnap::new(
            snap.date,
            snap.stats.end_equity(init_equity),
        ));
    }
    for trial in trial_stats.iter().skip(1) {
        for (i, snap) in trial.iter().enumerate() {
            avg_history[i].equity += snap.stats.end_equity(init_equity);
        }
    }
    let num_events = trial_stats.len();
    for snap in avg_history.iter_mut() {
        snap.equity /= num_events as f32;
    }
    avg_history
}

pub fn trial_summary(init_equity: f32, trial_stats: &Vec<BasketStats>) -> TrialBasketStatsSummary {
    let n_trials = trial_stats.len();
    let total_equity = trial_stats
        .iter()
        .fold(0., |sum, s| sum + init_equity + s.total_win - s.total_loss);
    let total_wins = trial_stats.iter().fold(0, |sum, s| sum + s.num_win);
    let total_won_amount = trial_stats.iter().fold(0., |sum, s| sum + s.total_win);
    let total_losses = trial_stats.iter().fold(0, |sum, s| sum + s.num_loss);
    let total_loss_amount = trial_stats.iter().fold(0., |sum, s| sum + s.total_loss);
    let total_n_trades = trial_stats
        .iter()
        .fold(0, |sum, s| sum + s.num_win + s.num_loss);

    let best_run = trial_stats
        .iter()
        .max_by(|s1, s2| {
            (s1.total_win - s2.total_loss)
                .partial_cmp(&(s2.total_win - s2.total_loss))
                .unwrap()
        })
        .unwrap();
    let worst_run = trial_stats
        .iter()
        .min_by(|s1, s2| {
            (s1.total_win - s2.total_loss)
                .partial_cmp(&(s2.total_win - s2.total_loss))
                .unwrap()
        })
        .unwrap();
    let average = BasketStatsSummary {
        equity: total_equity / n_trials as f32,
        avg_win: total_won_amount / total_wins as f32,
        avg_loss: total_loss_amount / total_losses as f32,
        num_wins: total_wins / n_trials as u32,
        num_losses: total_losses / n_trials as u32,
        num_trades: total_n_trades / n_trials as u32,
        pct_win: total_wins as f32 / total_n_trades as f32,
        pct_loss: total_losses as f32 / total_n_trades as f32,
    };

    let best = BasketStatsSummary::new(best_run, init_equity);
    let worst = BasketStatsSummary::new(worst_run, init_equity);
    TrialBasketStatsSummary {
        best,
        worst,
        average,
    }
}
