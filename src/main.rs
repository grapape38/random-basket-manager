extern crate chrono;
extern crate rand;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate clap;
extern crate csv;
extern crate serde_json;

use serde_json::{json, to_string_pretty, Map};
use std::error::Error;
use std::fs::{read_dir, File};
use std::io::Write;
use std::path::{Path, PathBuf};
use chrono::{NaiveDate, Duration};

pub mod basket;
use basket::*;
use clap::{App, Arg};

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("Basket Trading")
        .arg(Arg::with_name("file").short("f").takes_value(true))
        .arg(Arg::with_name("equity").short("e").takes_value(true))
        .arg(Arg::with_name("risk_pct").short("r").takes_value(true))
        .arg(Arg::with_name("num_trials").short("n").takes_value(true))
        .arg(Arg::with_name("output_dir").short("o").takes_value(true))
        .arg(Arg::with_name("dir").short("d").takes_value(true))
        .arg(Arg::with_name("interval").short("i").takes_value(true))
        .arg(
            Arg::with_name("plots")
                .long("plots")
                .help("Output csvs for plotting"),
        )
        .arg(Arg::with_name("verbose").short("v"))
        .get_matches();

    if !matches.is_present("file") && !matches.is_present("dir") {
        return Err("Provide either a file with -f or a folder of csv files with -d".into());
    }

    let equity = str::parse::<f32>(matches.value_of("equity").unwrap_or("100000."))
        .map_err(|e| format!("Error parsing Equity: {:?}", e))?;
    let risk_pct = str::parse::<f32>(matches.value_of("risk_pct").unwrap_or("1."))
        .map_err(|e| format!("Error parsing Risk Percent: {:?}", e))?;
    let n_trials = str::parse::<u32>(matches.value_of("num_trials").unwrap_or("1"))
        .map_err(|e| format!("Error parsing Number of Trials: {:?}", e))?;
    let interv = matches.value_of("interval").map(|i| 
        str::parse::<i64>(i)).map_or(Ok(None), |v| v.map(Some))?;

    let output_dir = matches
        .value_of("output_dir")
        .map(|d| format!("{}/", d))
        .unwrap_or("".to_string());
    let plot = matches.is_present("plots");
    let verbose = matches.is_present("verbose");
    if let Some(file_path) = matches.value_of("file") {
        let path = Path::new(file_path);
        run_single(equity, risk_pct, n_trials, interv, verbose, &output_dir, plot, &path)?;
    }
    else if let Some(folder) = matches.value_of("dir") {
        let mut files: Vec<PathBuf> = Vec::new();
        for entry in read_dir(folder)? {
            if let Ok(entry) = entry {
                if let Some(entry_path_str) = entry.path().as_path().as_os_str().to_str() {
                    if entry_path_str.matches("trades.csv").next().is_some() {
                        files.push(entry.path());
                    }
                }
            }
        }
        run_several(equity, risk_pct, n_trials, interv, &output_dir, plot, &files)?;
    }
    Ok(())
}

fn run_single(
    equity: f32, risk_pct: f32, n_trials: u32, day_interval: Option<i64>, verbose: bool, output_dir: &str, plot: bool, path: &Path)
-> Result<(), Box<dyn Error>> {
    let mut csv = csv::Reader::from_path(path)?;
    let mut trades: Vec<Trade> = Vec::new();
    for record in csv.deserialize() {
        if let Ok(t) = record {
            trades.push(t);
        }
    }
    let basket = BasketState::new(equity, risk_pct);
    let init_equity = basket.equity;
    let history = run_trials(basket, &trades, n_trials);
    let trial_stats: Vec<BasketStats> = history
        .iter()
        .map(|h| h.last().unwrap().stats.clone())
        .collect();
    let (start_date, end_date) = (
        history[0][0].get_date(), 
        history[0][history[0].len() - 1].get_date());
    let interval_info = IntervalInfo::new(start_date, end_date, day_interval);
    let strategy_name = path
        .file_stem()
        .and_then(|oss| oss.to_str())
        .ok_or("Could not extract strategy file name")?;
    let file_prefix = format!("{}-{}-{}", strategy_name, init_equity, risk_pct);
    save_results_single(
        init_equity,
        &history,
        &trial_stats,
        output_dir,
        &file_prefix,
        verbose
    )?;
    if plot {
        let line_fname = format!("{}_lines.csv", file_prefix);
        let density_fname = format!("{}_density.csv", file_prefix);
        create_markdown_single(
            n_trials,
            &line_fname,
            &density_fname,
            output_dir,
            &interval_info
        )?;
    }
    let summ = trial_summary(equity, &trial_stats);
    println!("{}", to_string_pretty(&json!(summ))?);
    Ok(())
}

fn run_several(equity: f32, risk_pct: f32, n_trials: u32, day_interval: Option<i64>, output_dir: &str, plot: bool, files: &Vec<PathBuf>)
-> Result<(), Box<dyn Error>> {
    let mut avg_baskets: Vec<AvgBasketHistory> = Vec::new();
    let mut end_equities: Vec<Vec<f32>> = Vec::new();
    let mut trial_summaries = Map::new();
    let mut strategy_names: Vec<&str> = Vec::new();
    for pathbuf in files {
        let path = pathbuf.as_path();
        let mut csv = csv::Reader::from_path(path)?;
        let mut trades: Vec<Trade> = Vec::new();
        for record in csv.deserialize() {
            if let Ok(t) = record {
                trades.push(t);
            }
        }
        let basket = BasketState::new(equity, risk_pct);
        let init_equity = basket.equity;
        let history = run_trials(basket, &trades, n_trials);
        let trial_stats: Vec<BasketStats> = history
            .iter()
            .map(|h| h.last().unwrap().stats.clone())
            .collect();

        avg_baskets.push(avg_trials(init_equity, &history));
        end_equities.push(
            trial_stats
                .iter()
                .map(|t| t.end_equity(init_equity))
                .collect(),
        );
        let strategy_name = path
            .file_stem()
            .and_then(|oss| oss.to_str())
            .ok_or("Could not extract strategy file name")?;
        trial_summaries.insert(
            String::from(strategy_name),
            json!(trial_summary(init_equity, &trial_stats)),
        );
        strategy_names.push(strategy_name);
    }

    let (start_date, end_date) = (
        avg_baskets[0][0].get_date(), 
        avg_baskets[0][avg_baskets[0].len() - 1].get_date());
    let interval_info = IntervalInfo::new(start_date, end_date, day_interval);

    save_avg_results(&avg_baskets, &end_equities, &output_dir, &strategy_names)?;
    if plot {
        create_markdown_several(n_trials, &interval_info, &output_dir)?;
    }
    println!("{}", to_string_pretty(&trial_summaries)?);
    Ok(())
}

fn save_avg_results(
    trial_history: &Vec<AvgBasketHistory>,
    end_equities: &Vec<Vec<f32>>,
    output_dir: &str,
    strategy_names: &Vec<&str>,
) -> Result<(), Box<dyn Error>> {
    let mut wtr = csv::Writer::from_path(format!("{}density.csv", output_dir))?;
    wtr.write_record(&["End Equity", "Strategy Name"])?;
    for (eq_list, strategy) in end_equities.iter().zip(strategy_names) {
        for eq in eq_list {
            wtr.serialize((eq, strategy))?;
        }
    }
    wtr.flush()?;
    let mut wtr = csv::Writer::from_path(format!("{}lines.csv", output_dir))?;
    wtr.write_record(&["Average Total Equity", "Date", "Strategy Name"])?;
    for (h, strategy) in trial_history.iter().zip(strategy_names) {
        for snap in h {
            wtr.serialize((snap.equity, snap.date, strategy))?;
        }
    }
    wtr.flush()?;
    Ok(())
}

fn save_results_single(
    init_equity: f32,
    trial_history: &Vec<BasketHistory>,
    trial_stats: &Vec<BasketStats>,
    output_dir: &str,
    file_prefix: &str,
    verbose: bool,
) -> Result<(), Box<dyn Error>> {
    if verbose {
        for (i, t) in trial_stats.iter().enumerate() {
            println!("Trial {}:", i + 1);
            t.print(init_equity);
            println!();
        }
    }
    let density_fname = format!("{}_density.csv", file_prefix);
    let mut wtr = csv::Writer::from_path(format!("{}{}", output_dir, density_fname))?;
    wtr.write_record(&["End Equity", "Trial"])?;
    for (num_trial, stats) in trial_stats.iter().enumerate() {
        wtr.serialize((stats.end_equity(init_equity), num_trial))?;
    }
    wtr.flush()?;
    let line_fname = format!("{}_lines.csv", file_prefix);
    let mut wtr = csv::Writer::from_path(format!("{}{}", output_dir, line_fname))?;
    wtr.write_record(&["Total Equity", "Date", "Trial"])?;
    for (i, h) in trial_history.iter().enumerate() {
        for snap in h {
            wtr.serialize((snap.stats.end_equity(init_equity), snap.date, i))?;
        }
    }
    Ok(())
}

fn add_r_block(s: &str) -> String {
    format!("```{{r}}\n{}\n```\n", s)
}

const R_BLOCK_START: &'static str = "```{r}\n";
const R_BLOCK_END: &'static str = "\n```\n";

fn create_markdown_several(n_trials: u32, interval: &IntervalInfo, output_dir: &str) -> Result<(), Box<dyn Error>> {
    let mut f = File::create(format!("{}report.Rmd", output_dir))?;
    let template = format!(
        include_str!("../markdown/report_template.rmd"),
        n = n_trials
    );
    f.write_all(template.as_bytes())?;
    let load_csv = add_r_block(&format!(
        include_str!("../markdown/load_csv.r"),
        lines_fname = "lines.csv",
        density_fname = "density.csv"
    ));
    f.write_all(load_csv.as_bytes())?;
    let date_ranges = get_date_ranges(interval);
    {
        let start_date = format!("{}", date_ranges[0]);
        let end_date = format!("{}", date_ranges[date_ranges.len() - 1]);
        let mut summary = String::new();
        summary.push_str(R_BLOCK_START);
        summary.push_str(&format!(
            include_str!("../markdown/lineplot.r"),
            var_name = "lines_filtered",
            start_date = start_date,
            end_date = end_date,
            breaks = "year",
            equity_column_name = "Average.Total.Equity",
            group_name = "Strategy.Name",
        ));
        summary.push_str(&format!(
            include_str!("../markdown/densityplot.r"),
            n = n_trials
        ));
        summary.push_str(R_BLOCK_END);
        f.write_all(summary.as_bytes())?;
    }
    for i in 0..date_ranges.len() - 1 {
        let start_date = format!("{}", date_ranges[i]);
        let end_date = format!("{}", date_ranges[i + 1]);
        let line_plot_str = add_r_block(&format!(
            include_str!("../markdown/lineplot.r"),
            var_name = "lines_filtered",
            start_date = start_date,
            end_date = end_date,
            breaks = interval.breaks(),
            equity_column_name = "Average.Total.Equity",
            group_name = "Strategy.Name",
        ));
        f.write_all(line_plot_str.as_bytes())?;
        let bar_plot_str = add_r_block(&format!(
            include_str!("../markdown/barplot.r"),
            group_name = "Strategy.Name",
            var_name = "lines_filtered",
            start_date = start_date,
            end_date = end_date,
            equity_column_name = "Average.Total.Equity",
        ));
        f.write_all(bar_plot_str.as_bytes())?;
    }
    Ok(())
}

fn create_markdown_single(
    n_trials: u32,
    line_fname: &str,
    density_fname: &str,
    output_dir: &str,
    interval: &IntervalInfo
) -> Result<(), Box<dyn Error>> {
    let mut f = File::create(format!("{}report.Rmd", output_dir))?;
    let template = format!(
        include_str!("../markdown/report_template.rmd"),
        n = n_trials
    );
    f.write_all(template.as_bytes())?;
    let load_csv = add_r_block(&format!(
        include_str!("../markdown/load_csv.r"),
        lines_fname = line_fname,
        density_fname = density_fname
    ));
    f.write_all(load_csv.as_bytes())?;
    let date_ranges = get_date_ranges(interval);
    //println!("{:?} {:?}", interval, date_ranges);
    for i in 0..date_ranges.len() - 1 {
        let start_date = format!("{}", date_ranges[i]);
        let end_date = format!("{}", date_ranges[i + 1]);
        let line_plot_str = add_r_block(&format!(
            include_str!("../markdown/lineplot.r"),
            var_name = "lines_filtered",
            start_date = start_date,
            end_date = end_date,
            breaks = interval.breaks(),
            equity_column_name = "Total.Equity",
            group_name = "Trial",
        ));
        f.write_all(line_plot_str.as_bytes())?;
    }
    let density_plot_str = add_r_block(&format!(
        include_str!("../markdown/density_one.r"),
        n = n_trials
    ));
    f.write_all(density_plot_str.as_bytes())?;
    Ok(())
}

fn get_date_ranges(info: &IntervalInfo) -> Vec<NaiveDate>
{
    let mut dates = Vec::new();
    let tot_days = (info.end_date - info.start_date).num_days();
    if let Some(day_interval) = info.day_interval {
        for day_inc in (0..tot_days + 1).step_by(day_interval as usize + 1) {
            dates.push(info.start_date + Duration::days(day_inc));
        }
        dates.push(info.end_date + Duration::days(1));
    }
    else {
        dates.push(info.start_date);
        dates.push(info.end_date + Duration::days(1));
    }
    dates
}


